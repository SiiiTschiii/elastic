module gitlab.ethz.ch/chgerber/elastic

go 1.12

require (
	cloud.google.com/go v0.39.0 // indirect
	github.com/asticode/go-astisub v0.0.0-20190514140258-c0ed7925c393
	github.com/aws/aws-sdk-go v1.21.0
	github.com/fortytw2/leaktest v1.3.0 // indirect
	github.com/golang/mock v1.3.1 // indirect
	github.com/google/btree v1.0.0 // indirect
	github.com/google/pprof v0.0.0-20190515194954-54271f7e092f // indirect
	github.com/imdario/mergo v0.3.7 // indirect
	github.com/kr/pty v1.1.4 // indirect
	github.com/mackerelio/go-osstat v0.0.0-20190712033226-e9f3fb840708 // indirect
	github.com/mailru/easyjson v0.0.0-20190626092158-b2ccc519800e // indirect
	github.com/olivere/elastic v6.2.21+incompatible
	github.com/sha1sum/aws_signing_client v0.0.0-20170514202702-9088e4c7b34b
	github.com/sirupsen/logrus v1.4.2
	github.com/streadway/amqp v0.0.0-20190404075320-75d898a42a94 // indirect
	github.com/stretchr/objx v0.2.0 // indirect
	github.com/stretchr/testify v1.3.0
	gitlab.ethz.ch/chgerber/annotation v1.0.0
	gitlab.ethz.ch/chgerber/annotation/v2 v2.0.0
	go.mongodb.org/mongo-driver v1.0.4
	go.opencensus.io v0.22.0 // indirect
	golang.org/x/crypto v0.0.0-20191029031824-8986dd9e96cf // indirect
	golang.org/x/exp v0.0.0-20190510132918-efd6b22b2522 // indirect
	golang.org/x/image v0.0.0-20190523035834-f03afa92d3ff // indirect
	golang.org/x/lint v0.0.0-20190930215403-16217165b5de // indirect
	golang.org/x/mobile v0.0.0-20190509164839-32b2708ab171 // indirect
	golang.org/x/mod v0.1.0 // indirect
	golang.org/x/net v0.0.0-20191028085509-fe3aa8a45271 // indirect
	golang.org/x/sync v0.0.0-20190911185100-cd5d95a43a6e // indirect
	golang.org/x/sys v0.0.0-20191028164358-195ce5e7f934 // indirect
	golang.org/x/time v0.0.0-20190308202827-9d24e82272b4 // indirect
	golang.org/x/tools v0.0.0-20191029041327-9cc4af7d6b2c // indirect
	golang.org/x/xerrors v0.0.0-20191011141410-1b5146add898 // indirect
	google.golang.org/grpc v1.21.1 // indirect
	honnef.co/go/tools v0.0.0-20190604153307-63e9ff576adb // indirect
)
