package main

import (
	"gitlab.ethz.ch/chgerber/elastic/go/elastic6"
	"log"
	"os"
)

func main() {

	client, err := elastic6.NewElasticClient(os.Getenv("ES_URL_TEST"))
	if err != nil {
		log.Fatal(err)
	}

	err = elastic6.CreateAnnotationIndex("dev.thebigbangtheory-s3-hls", client)
	if err != nil {
		log.Fatal(err)
	}
}
