package elastic7

// MappingAnnotationIdx is the Elasticsearch v7 index mapping and setting for the Annotation index.
var MappingAnnotationIdx = `{
    "mappings": {
		"properties": {
			"src": {
				"type": "text",
				"fields": {
					"keyword": {
						"type": "keyword",
						"ignore_above": 256
					}
				}
			},
			"subtitle": {
				"properties": {
					"count": {
						"type": "long"
					},
					"end": {
						"type": "long"
					},
					"segments": {
						"properties": {
							"end": {
								"type": "long"
							},
							"start": {
								"type": "long"
							},
							"text": {
								"type": "text",
								"fields": {
									"keyword": {
										"type": "keyword",
										"ignore_above": 256
									}
								}
							}
						}
					},
					"start": {
						"type": "long"
					},
					"text": {
						"type": "text",
						"fields": {
							"keyword": {
								"type": "keyword",
								"ignore_above": 256
							},
							"words_count": {
								"type": "token_count",
								"analyzer": "standard"
							}
						},
						"analyzer": "english_with_stopwords"
					}
				}
			},
			"usage": {
				"type": "nested",
				"properties": {
					"text": {
						"type": "text",
						"fields": {
							"keyword": { 
								"type": "keyword",
								"ignore_above": 256
							},
							"words_count": {
								"type": "token_count",
								"analyzer": "standard"
							}
						},
						"analyzer": "english_with_stopwords"
					},
					"shared": {
						"type": "long"
					}
				}
			},
			"playlist": {
				"properties": {
					"id": {
						"type": "keyword"
					},
					"name": {
						"type": "text",
						"fields": {
							"keyword": {
								"type": "keyword",
								"ignore_above": 256
							}
						}
					}
				}
			}
		}
    },
    "settings": {
        "analysis": {
            "filter": {
                "english_stemmer": {
                    "type": "stemmer",
                    "language": "english"
                },
                "english_possessive_stemmer": {
                    "type": "stemmer",
                    "language": "possessive_english"
                }
            },
            "analyzer": {
                "english_with_stopwords": {
                    "filter": [
                        "english_possessive_stemmer",
                        "lowercase",
                        "english_stemmer"
                    ],
                    "tokenizer": "standard"
                }
            }
        }
    }
}`
