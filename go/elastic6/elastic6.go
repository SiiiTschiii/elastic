package elastic6

import (
	"context"
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	v4 "github.com/aws/aws-sdk-go/aws/signer/v4"
	"github.com/olivere/elastic"
	"github.com/sha1sum/aws_signing_client"
	"gitlab.ethz.ch/chgerber/annotation/v2"
)

// mappingAnnotationIdx is the Elasticsearch v6 index mapping and setting for the Annotation index.
var mappingAnnotationIdx = `{
    "mappings": {
        "_doc": {
            "properties": {
                "src": {
                    "type": "text",
                    "fields": {
                        "keyword": {
                            "type": "keyword",
                            "ignore_above": 256
                        }
                    }
                },
                "subtitle": {
                    "properties": {
                        "count": {
                            "type": "long"
                        },
                        "end": {
                            "type": "long"
                        },
                        "segments": {
                            "properties": {
                                "end": {
                                    "type": "long"
                                },
                                "start": {
                                    "type": "long"
                                },
                                "text": {
                                    "type": "text",
                                    "fields": {
                                        "keyword": {
                                            "type": "keyword",
                                            "ignore_above": 256
                                        }
                                    }
                                }
                            }
                        },
                        "start": {
                            "type": "long"
                        },
                        "text": {
                            "type": "text",
                            "fields": {
                                "keyword": {
                                    "type": "keyword",
                                    "ignore_above": 256
                                },
                                "words_count": {
                                    "type": "token_count",
                                    "analyzer": "standard"
                                }
                            },
                            "analyzer": "english_with_stopwords"
                        }
                    }
                },
                "usage": {
                    "type": "nested",
                    "properties": {
                        "text": {
                            "type": "text",
                            "fields": {
                                "keyword": { 
                                    "type": "keyword",
                                    "ignore_above": 256
                                },
                                "words_count": {
                                    "type": "token_count",
                                    "analyzer": "standard"
                                }
                            },
                            "analyzer": "english_with_stopwords"
                        },
                        "shared": {
                            "type": "long"
                        }
                    }
                },
				"playlist": {
                    "properties": {
                        "id": {
                            "type": "keyword"
                        },
                        "name": {
                            "type": "text",
                            "fields": {
                                "keyword": {
                                    "type": "keyword",
                                    "ignore_above": 256
                                }
                            }
                        }
                    }
                }
            }
        }
    },
    "settings": {
        "analysis": {
            "filter": {
                "english_stemmer": {
                    "type": "stemmer",
                    "language": "english"
                },
                "english_possessive_stemmer": {
                    "type": "stemmer",
                    "language": "possessive_english"
                }
            },
            "analyzer": {
                "english_with_stopwords": {
                    "filter": [
                        "english_possessive_stemmer",
                        "lowercase",
                        "english_stemmer"
                    ],
                    "tokenizer": "standard"
                }
            }
        }
    }
}`

// NewElasticAWSClient connects to a aws elasticsearch instance at the given url
// Make sure the env variables AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY are set
func NewElasticAWSClient(url string) (awsClient *elastic.Client, err error) {

	session := session.Must(session.NewSessionWithOptions(session.Options{
		Config: aws.Config{Region: aws.String("eu-central-1")},
	}))

	_, err = session.Config.Credentials.Get()
	if err != nil {
		return nil, err
	}

	signer := v4.NewSigner(session.Config.Credentials)
	httpClient, err := aws_signing_client.New(signer, nil, "es", *session.Config.Region)
	if err != nil {
		return nil, err
	}

	// Create a client
	awsClient, err = elastic.NewClient(
		elastic.SetURL(url),
		elastic.SetSniff(false),
		elastic.SetHealthcheck(false),
		elastic.SetHttpClient(httpClient),
	)

	if err != nil {
		return nil, err
	}

	return awsClient, nil

}

// NewElasticClient connects to an elasticsearch instance at the given url
func NewElasticClient(url string) (esClient *elastic.Client, err error) {

	// Create a client
	esClient, err = elastic.NewClient(
		elastic.SetURL(url),
		elastic.SetSniff(false),
		elastic.SetHealthcheck(false),
	)

	if err != nil {
		return nil, err
	}

	return esClient, nil

}

// CreateAnnotationIndex creates a new Index with mappingAnnotationIdx
func CreateAnnotationIndex(name string, client *elastic.Client) error {

	createResult, err := client.CreateIndex(name).BodyString(mappingAnnotationIdx).Do(context.Background())
	if err != nil {
		return err
	}

	if !createResult.Acknowledged {
		return fmt.Errorf("index creation of index %s not acknowledged", name)
	}
	return nil
}

// AddAnnotation indexes an Annotation
func AddAnnotation(annotation *annotation.Annotation, index string, client *elastic.Client) error {

	_, err := client.Index().
		Index(index).
		Type("_doc").
		Id(annotation.ID.Hex()).
		BodyJson(annotation).
		Do(context.Background())

	if err != nil {
		return err
	}

	return nil
}

// AddAnnotations indexes an array of Annotation
func AddAnnotations(annotations []*annotation.Annotation, index string, client *elastic.Client) error {
	for _, a := range annotations {
		err := AddAnnotation(a, index, client)
		if err != nil {
			return err
		}
	}

	return nil
}
