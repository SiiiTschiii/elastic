package elastic6

import (
	"context"
	"encoding/json"
	"github.com/asticode/go-astisub"
	"github.com/olivere/elastic"
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.ethz.ch/chgerber/annotation/v2"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"os"
	"reflect"
	"strings"
	"testing"
	"time"
)

var mappingSimpleElastic68 = `
{
	"mappings" : {
        "_doc" : {
            "properties" : {
                "field1" : { "type" : "text" }
            }
        }
    }
}
`

var esURL = os.Getenv("ES_URL_TEST")
var testIdx = "dev.test"

func TestElasticAuthentication(t *testing.T) {

	client, err := NewElasticClient(esURL)
	if err != nil {
		t.Error(err)
	}

	resp, err := client.CatIndices().Do(context.Background())
	assert.Equal(t, nil, err)
	assert.Equal(t, true, len(resp) >= 1)

	// Search with a term query
	termQuery := elastic.NewBoolQuery().Must(elastic.NewMatchPhraseQuery("subtitle.text", "thank you"))

	result, err := client.Search().
		Index(testIdx).   // search in index "tweets"
		Query(termQuery). // specify the query
		//Sort("user.keyword", true). // sort by "user" field, ascending
		From(0).Size(0).         // take documents 0-9
		Pretty(true).            // pretty print request and response JSON
		Do(context.Background()) // execute

	assert.Equal(t, nil, err)
	assert.Equal(t, int64(41), result.TotalHits())
}

func TestElasticVersion(t *testing.T) {

	clientRemote, err := NewElasticClient(esURL)
	assert.Equal(t, nil, err)

	version, err := clientRemote.ElasticsearchVersion(esURL)
	assert.Equal(t, nil, err)
	assert.Equal(t, "6.8.0", version)

}

func TestCreateAndDeleteSimpleIndexAWS(t *testing.T) {

	clientRemote, err := NewElasticClient(esURL)
	assert.Equal(t, nil, err)

	createResult, err := clientRemote.CreateIndex("hdsiuof").BodyString(mappingSimpleElastic68).Do(context.Background())
	if err != nil {
		t.Error(err)
	}

	assert.Equal(t, true, createResult.Acknowledged)

	deleteResult, err := clientRemote.DeleteIndex("hdsiuof").Do(context.Background())
	assert.Equal(t, nil, err)
	assert.Equal(t, true, deleteResult.Acknowledged)
}

func TestCreateAndDeleteAnnotationIndexAWS(t *testing.T) {

	clientRemote, err := NewElasticClient(esURL)
	assert.Equal(t, nil, err)

	err = CreateAnnotationIndex("hdsiuof", clientRemote)
	assert.Equal(t, nil, err)

	deleteResult, err := clientRemote.DeleteIndex("hdsiuof").Do(context.Background())
	assert.Equal(t, nil, err)
	assert.Equal(t, true, deleteResult.Acknowledged)
}

func TestQueryAnnotationAndUnmarshal(t *testing.T) {

	clientRemote, err := NewElasticClient(esURL)
	assert.Equal(t, nil, err)

	// Search with a term query
	termQuery := elastic.NewTermQuery("_id", "5d317cc4ed5f48315275c130")
	searchResult, err := clientRemote.Search().
		Index(testIdx).          // search in index "tweets"
		Query(termQuery).        // specify the query
		From(0).Size(9).         // take documents 0-9
		Pretty(true).            // pretty print request and response JSON
		Do(context.Background()) // execute

	if err != nil {
		t.Error(err)
	}

	log.Debugf("Query took %d milliseconds\n", searchResult.TookInMillis)

	assert.Equal(t, int64(1), searchResult.TotalHits())

	// Here's how you iterate through results with full control over each step.
	if searchResult.Hits.TotalHits > 0 {

		// Iterate through results
		for _, hit := range searchResult.Hits.Hits {
			// hit.Index contains the name of the index

			// Deserialize hit.Source into a Tweet (could also be just a map[string]interface{}).
			var a annotation.Annotation
			bytes, err := hit.Source.MarshalJSON()
			assert.Equal(t, nil, err)

			err = json.Unmarshal(bytes, &a)
			assert.Equal(t, nil, err)

			id, err := primitive.ObjectIDFromHex(hit.Id)
			assert.Equal(t, nil, err)
			a.ID = id

			assert.Equal(t, "5d317cc4ed5f48315275c130", a.ID.Hex())

		}
	} else {
		// No hits
		log.Debug("Found no annotations\n")
	}

	var ttyp annotation.Annotation
	for _, item := range searchResult.Each(reflect.TypeOf(ttyp)) {
		if a, ok := item.(annotation.Annotation); ok {
			log.Debug(a)
		}
	}

}

var srtSubSample = `
1
00:00:00,100 --> 00:00:02,800
So if a photon is directed
through a plane with two slits in it

2
00:00:02,900 --> 00:00:05,500
and either slit is observed,
it will not go through both slits.

3
00:00:05,600 --> 00:00:06,800
If it's unobserved, it will.

`

func TestAddAndRetrieveAnnotation(t *testing.T) {

	sub, err := astisub.ReadFromSRT(strings.NewReader(srtSubSample))
	assert.Equal(t, nil, err)

	annos := annotation.SubToAnnotation(sub, "test")

	client, err := NewElasticClient(esURL)
	assert.Equal(t, nil, err)

	err = AddAnnotation(annos[0], "test999", client)
	if err != nil {
		t.Error(err)
	}

	defer client.DeleteIndex("test999")

	// Wait for server indexing to complete
	time.Sleep(1000 * time.Millisecond)

	// Search with a term query
	termQuery := elastic.NewTermQuery("_id", annos[0].ID.Hex())
	searchResult, err := client.Search().
		Index("test999").
		Query(termQuery).
		From(0).Size(9).
		Do(context.Background())

	if err != nil {
		t.Error(err)
	}

	assert.Equal(t, int64(1), searchResult.TotalHits())

	// Deserialize hit.Source into a Tweet (could also be just a map[string]interface{}).
	var a annotation.Annotation
	bytes, err := searchResult.Hits.Hits[0].Source.MarshalJSON()
	assert.Equal(t, nil, err)

	err = json.Unmarshal(bytes, &a)

	id, err := primitive.ObjectIDFromHex(searchResult.Hits.Hits[0].Id)
	assert.Equal(t, nil, err)
	a.ID = id

	assert.Equal(t, nil, err)

	assert.Equal(t, true, reflect.DeepEqual(a, *annos[0]))
}
