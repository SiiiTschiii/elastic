[![pipeline status](https://gitlab.ethz.ch/chgerber/elastic/badges/master/pipeline.svg)](https://gitlab.ethz.ch/chgerber/elastic/commits/master)
[![coverage report](https://gitlab.ethz.ch/chgerber/elastic/badges/master/coverage.svg)](https://gitlab.ethz.ch/chgerber/elastic/commits/master)

# ElasticSearch

ElasticSearch related stuff


#### elastic search
check health
```bash
curl http://127.0.0.1:9200/_cat/health
```

add bulk data from file
```bash
curl -H "Content-Type: application/x-ndjson" -X POST "localhost:9200/shakespeare/_bulk?pretty" --data-binary @shakespeare.json
```

show index mapping
```bash
curl -X GET "localhost:9200/dev.thebigbangtheorys3streaming/_mapping?pretty"
```

list indices with number of docs and size
```bash
curl localhost:9200/_cat/indices
```

delete an index
```bash
curl -X DELETE "localhost:9200/test.thebigbangtheorystreaming"
```

# Monstache
Go deamon to sync elasticsearch with mongodb

##### run daemon
```bash
~/code/monstache/build/linux-amd64/monstache -f monstache/config.toml
```

# Amazon Elasticsearch Services
```
curl -XGET 'https://search-composey2-aqb7ljzwyojsadlxbw2gxf5td4.eu-central-1.es.amazonaws.com/_cat/indices?v'
```

### Bugs
##### How to fix ElasticSearch [FORBIDDEN/12/index read-only / allow delete (api)]
```bash
curl -X PUT "localhost:9200/_cluster/settings" -H 'Content-Type: application/json' -d'
{
  "transient": {
    "cluster.routing.allocation.disk.watermark.low": "30mb",
    "cluster.routing.allocation.disk.watermark.high": "20mb",
    "cluster.routing.allocation.disk.watermark.flood_stage": "10mb",
    "cluster.info.update.interval": "1m"
  }
}
'
```

```bash
curl -XPUT -H "Content-Type: application/json" http://localhost:9200/_all/_settings -d '{"index.blocks.read_only_allow_delete": null}'
```

# Elasticdump

## install
```bash
npm install elasticdump -g
```

### dump
```bash
elasticdump \
  --input=http://localhost:9200/dev.test \
  --output=testData/dev.test_analyzer.json \
  --type=analyzer

elasticdump \
  --input=http://localhost:9200/dev.test \
  --output=testData/dev.test_mapping.json \ 
  --type=mapping
  
  
elasticdump \
  --input=http://localhost:9200/dev.test \
  --output=testData/dev.test_data.json \
  --type=data
```

### load
```bash
elasticdump \
  --input=testData/dev.test_analyzer.json \
  --output=http://localhost:9200/dev.test \
  --type=analyzer

elasticdump \
  --input=testData/dev.test_mapping.json \
  --output=http://localhost:9200/dev.test \
  --type=mapping
  
elasticdump \
  --input=testData/dev.test_data.json \
  --output=http://localhost:9200/dev.test \
  --type=data
```
